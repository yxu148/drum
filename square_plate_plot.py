def plate_plt(choice,a,eig_vecs,N_pts):
    '''
    Parameters->
    choice= The choice for the normal modes
    a= The set containing the wavnumbers
    eig_vecs= The set of all eigenvectors
    N_pts= Number of points in the 'x' direction = Number of points in the 'y' direction
    '''

    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import cm
    from matplotlib.ticker import LinearLocator
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.animation as animation
    import matplotlib

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

    choice=int(choice)
    N_pts=int(N_pts)
    h=float(1/(N_pts-1))

    x=np.linspace(0,1,N_pts)
    y=np.linspace(0,1,N_pts)
    X,Y = np.meshgrid(x,y,indexing='ij')
    Z=np.zeros([N_pts,N_pts])

    index=np.linspace(1,N_pts-2,N_pts-2,dtype=int)

    for j in index:
        for i in index:
            Z[i][j]=eig_vecs[(N_pts-2)*(j-1)+i-1,choice]

    # print(x.size,y.size,Z.size)

    # Plot the surface.
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm)

    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.title(f'Square Plate: Normal Mode ($\omega$ ={a[choice]}; $c=1$)')

    plt.show()

    #End of plot and beginning of animation

    fps = 10 # frame per sec
    frn = 50 # frame number of the animation
    ans='n'

    ans=input('\n Would you like to see the animations for the normal mode? (y/n) \n')
    if ans=='y':
        zarray=np.zeros((N_pts,N_pts,frn))
        for i in range(frn):
            zarray[:,:,i] = Z*np.sin(a[choice]*i/frn)

        def update_plot(frame_number, zarray, plot):
            plot[0].remove()
            plot[0] = ax.plot_surface(X, Y, zarray[:,:,frame_number], cmap="coolwarm")

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        plot = [ax.plot_surface(x, y, zarray[:,:,0], color='0.75', rstride=1, cstride=1)]
        ax.set_zlim(-0.1,+0.1)
        ani = animation.FuncAnimation(fig, update_plot, frn, fargs=(zarray, plot), interval=1000/fps)

        fn = 'Standard Mode Animation'
        ani.save(fn+'.mp4',writer='ffmpeg',fps=fps)

########################### End of Function #################
