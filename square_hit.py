def hit(N,eig_vecs,w):
    '''
    Parameters->
    Input:
        N= Number of points in the 'x' direction = Number of points in the 'y' direction
    Output:
        The animation is saved in the code directory as an mp4 file
    '''
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import cm
    from matplotlib.ticker import LinearLocator
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.animation as animation
    import matplotlib

    N=int(N)
    h=float(1/(N-1)) #'h' is the distance between adjacent grid points
    psi=np.zeros((N-2)**2)
    a=np.zeros((N-2)**2)


    fps = 20 # frame per sec
    frn = 100 # frame number of the animation
    x=np.linspace(0,1,N)
    y=np.linspace(0,1,N)
    X,Y = np.meshgrid(x,y,indexing='ij')    #Creates the x-y grid of points that define the membrane

    for i in range(1,N-2):
        for j in range (1,N-2):
            r=((h*i-0.4)**2+(h*j-0.4)**2)**(1/2) #A slightly off-center strike
            if r<0.5:
                psi[(N-2)*(j-1)+(i-1)]=-np.exp(-20*r**2)
                # psi[(N-2)*(j-1)+(i-1)]=eig_vecs[(N-2)*(j-1)+(i-1),1]
            else:
                psi[(N-2)*(j-1)+(i-1)]=0
    for k in range(1,(N-2)**2):
        a[k]=np.dot(psi,eig_vecs[:,k])

    def hit_frame(t): #Returns the membrane configuration for every time 't'
        Z=np.zeros([N,N])
        psi_t=np.zeros((N-2)**2)

        for k in range(0,20):
            psi_t+=a[k]*eig_vecs[:,k]*(np.cos(w[k]*t))
        for j in index:
            for i in index:
                Z[i][j]=psi_t[(N-2)*(j-1)+(i-1)]

        return Z

    index=np.linspace(1,N-2,N-2,dtype=int)

    zarray=np.zeros((N,N,frn))
    for i in range(frn):
        zarray[:,:,i] =hit_frame(i/frn) #Configuration of the membrane at each time step

    def update_plot(frame_number, zarray, plot):
        plot[0].remove()
        plot[0] = ax.plot_surface(X, Y, zarray[:,:,frame_number], cmap="inferno")

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    plot = [ax.plot_surface(x, y, zarray[:,:,0], color='0.75', rstride=1, cstride=1)]
    ax.set_zlim(-0.75,+0.75)
    ani = animation.FuncAnimation(fig, update_plot, frn, fargs=(zarray, plot), interval=1000/fps)

    fn = 'Time evolution of a strike'
    ani.save(fn+'.mp4',writer='ffmpeg',fps=fps)

####################### End of Function #####################
