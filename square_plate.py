def main():
    '''
    This is the part of the code that finds the normal modes for a square plate (1 unit x 1 unit) that is
    fixed at the boundaries.
    Our eigenvalues are of the form -h^2*a^2 where 'h' is the distance between adjacent grid points and
    'a' is defined as w=a*c (i.e. it is the wavenumber).
    '''
    import matrix_organize as mo
    import numpy as np
    import square_plate_plot as sq_plot
    import square_hit as sq_hit
    from numpy import linalg as LA
    from numpy.linalg import inv

    print('\n ~~~~~~~~~~~~ Vibrating Rectangular Membrane ~~~~~~~~~~~~\n')

    #Finding the eigenvalues for the discretized Helmholtz Equation
    p=input('\n What would you like to see? \n a)Time evolution of a hit (modelled as a Gaussian Distribution)\n b)Standard modes of vibrations\n')

    N=int(input('\n Enter the number of points in the x and y directions: \n '))

    A=mo.sq_mat_org(N) #This is the matrix we want to find the eigenvalues and eigenvectors for

    #A is a symmetric matrix

    h=float(1/(N-1)) #'h' is the distance between adjacent grid points

    A_inv=inv(A) #Since our eigenvalues go as h^2, they will be tiny. Thus, it's easier to find the eigenvalues
                # of the inverse matrix

    eig_vals_inv,eig_vecs=LA.eigh(A_inv) #Find the eigenvalues of a real symmetric matrix A^-1

    eig_vals=1/eig_vals_inv #These are the eigenvalues of A
    a=np.sqrt(-eig_vals/(h**2)) #The possible wavenumbers
    #'a' is in ascending order

    if p=='a':
        sq_hit.hit(N,eig_vecs,a)

    if p=='b':
        ans=input('\n Would you like to see the 3D plots of the normal modes? (y/n) \n')
        while ans=='y':
            choice=int(input(f'\n Which normal mode would you like to see? Enter a value from 0 to {a.size -1}\n'))
            sq_plot.plate_plt(choice,a,eig_vecs,N)
            ans=input('\n Would you like to continue? (y/n) \n')

####################### End of Function #####################

main()
