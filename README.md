# Vibrating Rectangular Membrane



## File to execute

Running the following
```
python square_plate.py
```
should get the code started. Follow the prompts to get the desired outputs. The animations are stored as an .mp4 file in the directory where the repository is pulled. 
## List of Files
- square_plate.py 
- matrix_organize.py
- square_plate_plot.py
- square_hit.py
