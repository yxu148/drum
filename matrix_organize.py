def sq_mat_org(N):
    '''
    This function organizes the matrix which represents the linear equations approximating the partial derivatives.
    The linear equations need to be brought to the form Az=b. This function finds 'A' for a square boundary.

    Parameters->
    N = Number of points in the 'x' direction = Number of points in the 'y' direction

    Output->
    A- an (N-2) x (N-2) matrix
    '''

    import numpy as np

    N=int(N)

    #Number of unknowns = (N-2)^2

    A=np.zeros(((N-2)**2,(N-2)**2))

    # 'i' and 'm' go in the x-direction
    # 'j' and 'n' go in the y-direction

    index=np.linspace(1,N-2,N-2,dtype=int)

    for n in index:
        for m in index:
            if m==1 and n==1: #Bottom left corner
                for j in index:
                    for i in index:
                        if i==1 and j==1:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = -4
                        elif i==2 and j==1:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==1 and j==2:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1

            elif n==1 and m!=1 and m!=(N-2): #Bottom Row without corners
                for j in index:
                    for i in index:
                        if i==m and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = -4
                        elif i==(m-1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==(m+1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n+1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1

            elif n==1 and m==(N-2): #Bottom right corner
                for j in index:
                    for i in index:
                        if i==m and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = -4
                        elif i==(m-1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n+1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1

            elif m==1 and n==(N-2): #Top left corner
                for j in index:
                    for i in index:
                        if i==m and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = -4
                        elif i==(m+1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n-1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1

            elif m==(N-2) and n==(N-2): #Top right corner
                for j in index:
                    for i in index:
                        if i==m and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = -4
                        elif i==(m-1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n-1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1

            elif m==1 and n!=1 and n!=(N-2): #Left column without corners
                for j in index:
                    for i in index:
                        if i==m and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = -4
                        elif i==(m+1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n-1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n+1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1

            elif m==(N-2) and n!=1 and n!=(N-2): #Right column without corners
                for j in index:
                    for i in index:
                        if i==m and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = -4
                        elif i==(m-1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n-1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n+1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1

            elif n==(N-2) and m!=1 and m!=(N-2): #Top row without corners
                for j in index:
                    for i in index:
                        if i==m and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = -4
                        elif i==(m-1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==(m+1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n-1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1

            else: #Interior points that have no boundary points as neighbors
                for j in index:
                    for i in index:
                        if i==m and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = -4
                        elif i==(m-1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==(m+1) and j==n:
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n-1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1
                        elif i==m and j==(n+1):
                            A[(N-2)*(n-1)+m-1][(N-2)*(j-1)+i-1] = 1



    return A

############################### End of Function #######################
